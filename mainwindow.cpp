#include <QDebug>
#include <QInputDialog>
#include <QFile>
#include <QTextStream>
#include <QFontDatabase>

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow), mTasks() {
    ui->setupUi(this);

    if (QFontDatabase::addApplicationFont(":/FontAwesome.otf") < 0)
        qWarning() << "FontAwesome cannot be loaded !";

    QFont font;
    font.setFamily("FontAwesome");
    font.setPixelSize(16);

    ui->styleModeButton->setFont(font);
    ui->styleModeButton->setText("\uf186");

    ui->addTaskButton->setFont(font);
    ui->addTaskButton->setText("\uf067");


    ui->styleModeButton->setCheckable(true);

    connect(ui->addTaskButton, &QPushButton::clicked, this, &MainWindow::addTask);
    connect(ui->taskTextBox, &QLineEdit::returnPressed, this, &MainWindow::addTask);
    connect(ui->styleModeButton, &QPushButton::toggled, this, &MainWindow::modeSwitched);
    updateStatus();
}

MainWindow::~MainWindow() {
    delete ui;
}


void MainWindow::addTask() {
    QString name = ui->taskTextBox->text();
    if (!name.isEmpty()) {
        // qDebug() << "addTask button clicked";
        Task* task = new Task(name);
        connect(task, &Task::removed, this, &MainWindow::removeTask);
        connect(task, &Task::statusChanged, this, &MainWindow::taskStatusChanged);
        mTasks.append(task);
        ui->tasksLayout->addWidget(task);
        updateStatus();
        ui->taskTextBox->clear();
    }
}

void MainWindow::removeTask(Task* task) {
    mTasks.removeOne(task);
    ui->tasksLayout->removeWidget(task);
    delete task;
    updateStatus();
}

void MainWindow::taskStatusChanged(Task* task) {
    updateStatus();
}

void MainWindow::updateStatus() {
    int completed = 0;
    for (auto& task : mTasks) {
        if (task->isComplete()) {
            ++completed;
        }
    }
    int remaining = mTasks.size() - completed;
    ui->statusLabel->setText(QString("Status: %1 to do / %2 completed").arg(remaining).arg(completed));
}

void MainWindow::modeSwitched(bool checked)
{
    if(checked) {
        if (QFontDatabase::addApplicationFont(":/FontAwesome.otf") < 0)
            qWarning() << "FontAwesome cannot be loaded !";

        QFont font;
        font.setFamily("FontAwesome");
        font.setPixelSize(16);

        ui->styleModeButton->setFont(font);
        ui->styleModeButton->setText("\uf185");

        // set stylesheet
        QFile file(":/dark.qss");
        file.open(QFile::ReadOnly | QFile::Text);
        QTextStream stream(&file);
        this->setStyleSheet(stream.readAll());

    } else {
        if (QFontDatabase::addApplicationFont(":/FontAwesome.otf") < 0)
            qWarning() << "FontAwesome cannot be loaded !";

        QFont font;
        font.setFamily("FontAwesome");
        font.setPixelSize(16);

        ui->styleModeButton->setFont(font);
        ui->styleModeButton->setText("\uf186");

        // set stylesheet
        QFile file(":/light.qss");
        file.open(QFile::ReadOnly | QFile::Text);
        QTextStream stream(&file);
        this->setStyleSheet(stream.readAll());
    }
}
