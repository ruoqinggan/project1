#include <QInputDialog>
#include <QDebug>
#include <QFontDatabase>

#include "task.h"
#include "ui_task.h"

Task::Task(const QString& name, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Task)
{
    ui->setupUi(this);

    if (QFontDatabase::addApplicationFont(":/FontAwesome.otf") < 0)
        qWarning() << "FontAwesome cannot be loaded !";

    QFont font;
    font.setFamily("FontAwesome");
    font.setPixelSize(16);

    ui->editButton->setFont(font);
    ui->editButton->setText("\uf044");

    ui->removeButton->setFont(font);
    ui->removeButton->setText("\uf1f8");

    setName(name);
    connect(ui->editButton, &QPushButton::clicked, [this] {
        emit editTask();
    });
    connect(ui->removeButton, &QPushButton::clicked, [this] {
        emit removed(this);
    });
    connect(ui->checkbox, &QCheckBox::toggled, this, &Task::checked);

    if (QFontDatabase::addApplicationFont(":/FontAwesome.otf") < 0)
        qWarning() << "FontAwesome cannot be loaded !";


}

Task::~Task()
{
    delete ui;
}

void Task::setName(const QString &name) {
    ui->checkbox->setText(name);
}

QString Task::name() const {
    return ui->checkbox->text();
}

bool Task::isComplete() const {
    return ui->checkbox->isChecked();
}

void Task::checked(bool checked) {
    QFont font(ui->checkbox->font());
    font.setStrikeOut(checked);
    ui->checkbox->setFont(font);
    emit statusChanged(this);
}

void Task::editTask() {
    bool ok;
    const QString oldName = name();
    QString name = QInputDialog::getText(this,
                                         tr("Edit Task"),
                                         tr("Task name"),
                                         QLineEdit::Normal,
                                         tr(oldName.toStdString().c_str()),
                                         &ok);
    if (ok && !name.isEmpty()) {
        // qDebug() << "addTask button clicked";
        setName(name);
    }
}
